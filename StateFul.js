import React from 'react';
import './StateFul.css';

class StateFul extends React.Component {

    constructor() {
        super(); // without super "this" won't work in constructor. 

        this.state = {
            name: "Vanellope", abc: "Hello", styleObj: {
                color: "red",
                fontSize: "24px",
                background: "Black"
            }
        } // can create state only in constructor
    }

    componentWillMount() {
        console.log("I'm in componentWillMount");
    }

    componentDidMount() {
        console.log("I'm in componentDidMount");
        this.setState({ name: "Mario" });
    }

    btnClick() {
        this.setState({ name: "Ralph", abc: "Nimo" });
        // this.props.villain = "NoNo";
    }

    render() {
        console.log("I'm in Render");
        return <div>
            <h3 style={this.state.styleObj}>Hello {this.state.name} {this.state.abc}</h3>
            <h4>Villain : {this.props.villain}</h4>
            <button id="myBtn" className="btn" onClick={() => this.btnClick()}>Click Me</button>
            {/* You can update state only by using setState */}
            {/* The difference b/w html events and react events is html events are lowercase and react events are camelCase and react events are Synthetic Events i.e. events will behave same in multiple browsers.*/}
        </div>
    }
}

export default StateFul;

// Main difference b/w Class based component and functional component is You can't use state in Functional Component.